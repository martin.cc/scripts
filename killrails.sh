#!/bin/sh
# Check that rails is not running

# Get the list of fails process ids
PLIST1=`pgrep -f rails`
PLIST2=`pgrep -f puma`


# replace newline with space (tr = translate)
PLIST1=`echo ${PLIST1} | tr "\n" " "`
PLIST2=`echo ${PLIST2} | tr "\n" " "`

PLIST=`echo ${PLIST1} ${PLIST2}`

# Equivlent to (PLIST = "" OR PLIST = " ")
if [ "${PLIST}" = ""  -o "${PLIST}" = " " ]
then
    echo "Rails/Puma is NOT running"
else
    echo "Terminating Rails/Puma process: ${PLIST}"
    echo "kill -9 ${PLIST}"
    kill -9 ${PLIST}
fi