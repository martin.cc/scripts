#!/usr/bin/env bash

PG_USER='middle8'
PG_DATABASE='m8-skills-development'
PG_HOST='localhost'

# this should be calculated - als should add the date
DUMP_LOCATION='/Users/cmartin/pg_dump_files/m8_skills_local.dump'

pg_dump -Fc --no-acl --no-owner -h "${PG_HOST}" -U "${PG_USER}" "${PG_DATABASE}" > "${DUMP_LOCATION}"