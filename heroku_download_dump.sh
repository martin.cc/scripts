#!/bin/bash

# Download the latest dump file from heroku and store it in the dumps directory

# ARGUMENTS
HEROKU_APP="$1"  # the name of the Heroku application
FULL_DUMP_FILE="$2"   # the fully qualified path to the dumpfile
DUMP_DIR="$(dirname "${FULL_DUMP_FILE}")"

echo "Starting: $(basename "$0")"

# VARIABLES
C_RED=
C_RESET=
# if stdout or stderr are not connected to a terminal
if [ -t 1 ] && [ -t 2 ] # test for stdin and stdout
then
    WGET_OPTIONS='--progress=bar:noscroll'
    # Set up colour support, if we have it
    if tput -V >/dev/null 2>&1
    then
        C_RED=$(tput setaf 1)
        C_RESET=$(tput sgr0)
    fi
else
    WGET_OPTIONS='--quiet' #--append-output=${LOGFILE}
fi

# set up the logfile before any calls to exit_error
LOGFILE="/tmp/$(basename "$0").log"

# clear out the logfile
echo 'Clearing Logfile ...'
rm -f "${LOGFILE}"

# if run from a project root direcory, try and load the project environment
[ -f ".project_environment.sh" ] && source ".project_environment.sh"

# OVER-RIDE VARIABLES, with those defined in the middle8_profile
[ -n "${LOCAL_PROFILE}" ] && [ -f "${LOCAL_PROFILE}" ] && source "${LOCAL_PROFILE}"

# NOTE the MAC doesnt support the --no-run-if-empty flag, but does the action by default, so we only need this for linux
XARGS_OPTIONS=''
if [ "$(uname -s)" = "Linux" ]
then
    XARGS_OPTIONS='--no-run-if-empty'
fi


#############################################################
# Functions
# These must ve defined before they are used
#############################################################

usage()
{
    echo
    echo "usage: $(basename "$0") heroku_app file_name_to_Import"
    echo 'options:'
    echo '-h   Help'
    echo
}

# Display error and exit
# $1: String to display
exit_error()
{
    # If there is an error we do not want to silence it
    echo >&2
    echo "${C_RED}Error:${C_RESET} $1" >&2
    echo "Error: $1" >> "${LOGFILE}"
    echo "Check ${LOGFILE} for more information"  >&2
    echo >&2
    #clean_up
    exit 1
}

download_heroku_dump()
{
    # Check we have a heroku api key
    [ -n "${HEROKU_API_KEY}" ] || exit_error "Set the \$HEROKU_API_KEY environment variable"

    # Check we are logged in to heroku
    HEROKU_USER=$(heroku auth:whoami) || exit_error 'You are not logged in to Heroku: Check you HEROKU_API_KEY is valid'

    # Check we have access to app specified in ${HEROKU_APP}
    (heroku apps | grep "${HEROKU_APP}" >>/dev/null 2>&1) || exit_error "Your id: ${HEROKU_USER} does not have access to the '${HEROKU_APP}' app"

    #clearing old partials
    echo "Clearing old partial dump files"
    rm -f "${DUMP_DIR}"/*.partial
    rm -f "${DUMP_DIR}"/*.dump

    # find the latest dump file
    echo "Locating Dump File on Heroku ..."
    DUMP_SOURCE=$(heroku pg:backups public-url -a "${HEROKU_APP}")
    echo "Dumpfile: ${FULL_DUMP_FILE}.partial" >> "${LOGFILE}"

    # fetch the file
    echo "Downloading the file"
    wget --output-document="${FULL_DUMP_FILE}.partial" --tries=12  "${WGET_OPTIONS}" --continue "${DUMP_SOURCE}" || exit_error 'Failed to complete download'

    # success - rename the file, and remove any old partials
    # moving file
    mv "${FULL_DUMP_FILE}.partial" "${FULL_DUMP_FILE}"
    rm -f "${DUMP_DIR}"/*.partial   # remove any other partials - dont prompt if there are none to remove
}

################################################################
#  Main
################################################################

# Create the directory if not present
mkdir -p "${DUMP_DIR}"

# if we already have the correct file - bail - we are done for today
# exit if the file exists
[ -f "${FULL_DUMP_FILE}" ] && echo "postgres dump file ${FULL_DUMP_FILE} is already present" && exit

# Check that the required commands are installed and other conditions are met
echo "Checking ..."
heroku --version >/dev/null 2>&1 || exit_error "heroku-cli is not installed:  Install it now !!!"
wget --version   >/dev/null 2>&1 || exit_error "wget is not installed:  Install it now !!!"

# do the download
download_heroku_dump

echo "Finished file download "
