#!/usr/bin/env bash

# This should hold all the shell environment variables used by the scripts
# it should be sourced by each script so that we only need to set the values in one place

export TODAY=$( date "+%Y-%m-%d" )

export PGHOST=${PGHOST:-"localhost"}
export PGPORT=${PGPORT:-5432}
export PGPASSFILE=${PGPASSFILE:-"${HOME}/.pgpass"}
export PGUSER=${PGUSER:-"default"}
export PGPASSWORD=${PGPASSWORD:-"default"}
export PGDATABASE=${PGDATABASE:-"default"}

#export PGUSER="$(ruby -r yaml -e 'puts YAML.load_file(ARGV[0], aliases: true)["development"]["username"]' ${PWD}/config/database.yml)"
#export PGPASSWORD="$(ruby -r yaml -e 'puts YAML.load_file(ARGV[0], aliases: true)["development"]["password"]' ${PWD}/config/database.yml)"
#export PGDATABASE="$(ruby -r yaml -e 'puts YAML.load_file(ARGV[0], aliases: true)["development"]["database"]' ${PWD}/config/database.yml)"


export HEROKU_APP=
export HEROKU_API_KEY=

export DUMP_DIR="${HOME}/pg_dump_files"
export DUMP_FILE="${PG_DATABASE}_raw_${TODAY}.dump"

export REPO_URL="http://192.168.0.251/dumps"  # Note, DO NOT include a training slash

export PG_AUTH_DATA="${PG_HOST}:${PG_PORT}:${PG_DATABASE}:${PG_USER}:${PG_PASSWORD}"

export LOGFILE="/tmp/$(basename "$0").log"

# the local profile is held outside of the project.
# it hold variables that the programmer may require to change locally.
# ie: override the settings above
#it is usually located in the home directory
export LOCAL_PROFILE="${HOME}/.app_name_profile"