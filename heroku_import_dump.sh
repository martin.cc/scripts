#!/bin/bash
#
# This script will import a postgreSQL database dump
#

echo "Starting: $(basename $0)"

# VARIABLES
export FULL_DUMP_FILE="$1"
DUMP_DIR="$(dirname "${FULL_DUMP_FILE}")"

C_RED=
C_RESET=
# Set up colour support, if we have it
if [ -t 1 -a -t 2 ]
then
  if tput -V >/dev/null 2>&1
  then
    C_RED=$( tput setaf 1 )
    C_RESET=$( tput sgr0 )
  fi
fi

# log file must be set early, before exit error is called
LOGFILE="/tmp/$(basename "$0").log"  # we can assume that the /tmp dir exists
export LOGFILE

# if run from a project root direcory, try and load the project environment
[ -f ".project_environment.sh" ] && source ".project_environment.sh"

# OVER-RIDE VARIABLES, with those defined in the middle8_profile
[ -n "${LOCAL_PROFILE}" ] && [ -f "${LOCAL_PROFILE}" ] && source "${LOCAL_PROFILE}"

# NOTE the MAC doesnt support the --no-run-if-empty flag, but does the action by default, so we only need this for linux
XARGS_OPTIONS=''
if [ "$(uname -s)" = "Linux" ]
then
    XARGS_OPTIONS='--no-run-if-empty'
fi

#############################################################
# Functions
# These must ve defined before they are used
#############################################################

usage()
{
    echo
    echo "usage: $(basename "$0") file_name_to_Import"
    echo 'options:'
    echo '-h   Help'
    echo
}

# Display error and exit
# $1: String to display
exit_error()
{
    echo
    echo "${C_RED}Error:${C_RESET} $1"
    echo "Error: $1" >> "${LOGFILE}"
    echo "Check '${LOGFILE}' for more information"
    echo
    exit 1  #  This will call cleanup
}

# returns 1 if the database exists
check_db_exists()
{
  return "$( psql  --tuples-only --no-align --command "SELECT 1 FROM pg_database WHERE datname='${PGDATABASE}'" )"
}

################################
#  Main
################################

# Check that we are not root
#[ "$( id -u )" = "0" ] && exit_error "Please don't run this command as root!"

# Check that the postgresql dump file exists
[ -f "${FULL_DUMP_FILE}" ] || exit_error "Dumpfile: ${FULL_DUMP_FILE}, does not exist"

echo "Clearing Logfile ..."
rm -f "${LOGFILE}"

# Check that the required commands are installed and other conditions are met
# NOte: download tools wget, curl are checked in the download method
echo "Checking ..."
psql --version       >/dev/null 2>>"${LOGFILE}" || exit_error "Postgresql client (psql) is not installed:  Install it now !!!"
wget --version       >/dev/null 2>>"${LOGFILE}" || exit_error "the wget utility is not installed:  Install it now !!!"

# If the database exists, check if it is in use and if not drop it
if check_db_exists
then
  echo "checking if database: ${PGDATABASE} is in use"
  # here pgsql is not displaying the headers, and we are selecting only one value.  STDOUT will get the count only, but it will contain extra white space
  ROW_COUNT=$( psql --tuples-only --no-align --host localhost --username "${PGUSER}" --dbname "postgres" --command "SELECT count(*) FROM PGstat_activity WHERE datname='${PGDATABASE}' AND trim(application_name) != '' AND pid != PGbackend_pid();" )
  # remove white space
  ROW_COUNT="$(echo -e "${ROW_COUNT}" | tr -d '[:space]')"

  if [ "${ROW_COUNT}" != "0" ]
  then
      echo
      echo "The following applications are using the database:"
      # This query will detect and show which apps are using a postgres database, and their ids (excluding the current query connection)
      psql --host localhost --username "${PGUSER}" --dbname "postgres" --command "SELECT application_name, pid FROM PGstat_activity WHERE datname='${PGDATABASE}' AND trim(application_name) != '' AND pid != PGbackend_pid();"
      echo
      exit_error "Database in use: The above process must be terminated to import the database"
  fi
fi

# drop the database
echo "Removing the existing database: ${PGDATABASE} (if it exists)"
dropdb --if-exists "${PGDATABASE}" >> "${LOGFILE}" 2>&1 || exit_error "dropdb ${PGDATABASE} failed: Perhaps DB is in use, usually pgadmin3 or rails s/c"

# Attempt to drop the role if it exists exist
#echo "Removing the user role: ${PGUSER} (if it exists)"
#dropuser --if-exists "${PGUSER}" >> "${LOGFILE}" 2>&1  || exit_error "dropuser ${PGUSER} failed"

# Create the user.  Cant use the createuser utility because of password entry
# Using PSQL instead.
echo "Attempting to add user role: ${PGUSER}"
if psql --command "CREATE ROLE ${PGUSER} WITH SUPERUSER LOGIN PASSWORD '${PGPASSWORD}'" >> /dev/null 2>&1
then
  echo "Added user role: ${PGUSER}"
else
  echo "User role: ${PGUSER} already exists"
fi
#psql --command "CREATE ROLE ${PGUSER} WITH SUPERUSER LOGIN PASSWORD '${PGPASSWORD}'" >> /dev/null 2>&1  || exit_error "Unable to create user role: ${PGUSER}. Check logfile ${LOGFILE}"

# create the database
echo "Creating the database: ${PGDATABASE}"
createdb --owner="${PGUSER}" --encoding=utf8 "${PGDATABASE}" >> "${LOGFILE}" 2>&1 || exit_error "createdb failed"

echo "Restoring dump file to local DB  database=${PGDATABASE} user=${PGUSER} ..."
#pv -w 80 -p -t -e  -b "${DUMP_DIR}/${DUMP_FILE}" | pg_restore --clean --if-exists --no-owner --no-privileges --role="${PGUSER}" --dbname="${PGDATABASE}"  >> "${LOGFILE}" 2>&1 || exit_error "pg_restore failed"
cat "${FULL_DUMP_FILE}" | pg_restore --clean --if-exists --no-owner --no-privileges --role="${PGUSER}" --dbname="${PGDATABASE}" >> "${LOGFILE}" 2>&1 || exit_error "pg_restore failed"

echo "finished!"
