#!/bin/bash
#
# This script will import a Postgres database dump
#

# VARIABLES
C_RED=
C_RESET=
LOGFILE="/tmp/$(basename "$0").log"  # we can assume that the /tmp dir exists

echo "Starting: $(basename $0)"

[ -f ".project_environment.sh" ] && source ".project_environment.sh"

# NOTE the MAC doesnt support the --no-run-if-empty flag, but does the action by default, so we only need this for linux
XARGS_OPTIONS=''
if [ "$(uname -s)" = "Linux" ]
then
    XARGS_OPTIONS='--no-run-if-empty'
fi

usage()
{
    echo
    echo "usage: $(basename "$0") <options> <dump file>"
    echo 'options:'
    echo '-d   Disable_post_processing'
    echo '-h   Help'
    echo 'If the dumpfile is not specified, the select or download dialog is used'
    echo
}

# Display error and exit
# $1: String to display
exit_error()
{
    echo
    echo "${C_RED}Error:${C_RESET} $1"
    echo "Error: $1" >> "${LOGFILE}"
    echo "Check '${LOGFILE}' for more information"
    echo
    exit 1  #  This will call cleanup
}

download_from_repo()
{
    # test we have wget
    wget --version >/dev/null 2>>"${LOGFILE}" || exit_error "wget is not installed:  Install it now !!!"
    #curl --version       >/dev/null 2>>"${LOGFILE}" || exit_error "curl is not installed:  Install it now !!!"

    # get rid of any partial files
    rm -f "${DUMP_DIR}/${PGDATABASE}_"*".partial"

    DUMP_SOURCE="${REPO_URL}/${DUMP_FILE}"


    # try and get the dump from the local server
    echo "Attempting downloading Dump File from repo ..."
    echo "Dumpfile: ${DUMP_DIR}/${DUMP_FILE}" >> "${LOGFILE}"

    # Get the dump file from Repo
    # TODO:  Should also check the return value ie: MUST be 200 otherwise error
    wget --output-document "${DUMP_DIR}/${DUMP_FILE}.partial" --tries=5  --continue "${DUMP_SOURCE}" || exit_error "Failed to complete download"
    #curl --fail --continue-at - -o "${DUMP_DIR}/${DUMP_FILE}.partial" --retry 5  --progress-bar "${DUMP_SOURCE}"
}

download_from_heroku()
{
    # test we have wget
    echo "Checking  we have access to wget"
    wget --version >/dev/null 2>>"${LOGFILE}" || exit_error "wget is not installed:  Install it now !!!"

    # check if the heroku app is installed
    echo "Checking we have access to heroku"
    heroku --version >/dev/null 2>>"${LOGFILE}" || exit_error "heroku-cli is not installed:  Install it now !!!"

    # Check if we are logged in
    heroku auth:whoami > /dev/null 2>&1 || exit_error "You are not logged in to heroku"

    # check if we have access to the requested application
    heroku ps --app "${HEROKU_APP}" > /dev/null 2>&1 || exit_error "You do not have access to the heroku app '${HEROKU_APP}'"

    # try getting the dumpfile from Heroku
    echo "Locating Dump File on Heroku ..."
    DUMP_SOURCE=$( heroku pg:backups:url --app "${HEROKU_APP}" )

    echo "Clearing any partial downloads ..."
    # get rid of any partial files.  Leave today's as we can pick up where we left off or over write
    ls -1r "${DUMP_DIR}/${PGDATABASE}_"*".partial" 2>/dev/null | tail -n +2  | xargs ${XARGS_OPTIONS}  rm -f --

    # fetch the new dump
    echo "Downloading Dump File from Heroku..."
    wget --output-document "${DUMP_DIR}/${DUMP_FILE}.partial" --tries=5  --continue "${DUMP_SOURCE}" || exit_error "Failed to complete download"
}

select_download()
{
    while true
    do
        read -r -p "Do you want to download a new dump from (Repo/Heroku)? " yn
        case ${yn} in
            [Rr]* )
                download_from_repo || exit_error 'Failed to fetched dump from repo'
                break ;;
            [Hh]* )
                download_from_heroku  || exit_error 'Failed to fetched dump from Heroku'
                break ;;

            * ) echo "Please answer (R)Repo or (H)eroku.";;
        esac
    done

    # got a good download, so rename the file
    echo "Downloaded: Renaming to ${DUMP_DIR}/${DUMP_FILE}"
    mv "${DUMP_DIR}/${DUMP_FILE}.partial" "${DUMP_DIR}/${DUMP_FILE}"
}

# returns 1 if the database exists
check_db_exists()
{
  return "$( psql  --tuples-only --no-align --command "SELECT 1 FROM pg_database WHERE datname='${PGDATABASE}'" )"
}

################################
#  Main
################################

# OVER-RIDE VARIABLES, with those defined in the middle8_profile
# shellcheck source=/dev/null
[ -f "${LOCAL_PROFILE}" ] && source "${LOCAL_PROFILE}"

# Check that we are not root
#[ "$( id -u )" = "0" ] && exit_error "Please don't run this command as root!"

# Check we have a $HOME
[ -z "${HOME}" ] && exit_error "You don't have a HOME environment variable set"

FLAG_DISABLE_DOWN_LOAD_OR_SELECT=''
FLAG_DISABLE_POST_PROCESSING=''

while getopts dh: FLAG
do
    case "${FLAG}" in

        d)
            echo "Disable post processing"
            FLAG_DISABLE_POST_PROCESSING='true'
            ;;
        h)
            usage
            exit
            ;;
        ?)
            usage
            exit 1
            ;;
    esac
done

shift $(( OPTIND - 1 ))  # shift past the last flag or argument

# Override if an parameter was passed
if [ -n "$1" ]
then
    if [ -f "$1" ]
    then
        DUMP_DIR="$( dirname "$1" )"
        DUMP_FILE="$( basename "$1" )"
        echo 'Disable download or select'
        FLAG_DISABLE_DOWN_LOAD_OR_SELECT='true'
    else
        echo "File not found $1"
        usage
        exit 1
    fi
else
    # We are using the default or configured DUMP_DIR
    # create the DUMP_DIR directory if it doesn't exist already
    mkdir -p "${DUMP_DIR}"
    # Before starting get rid of any partial files that may already exist
    rm -f "${DUMP_DIR}/*.partial"
fi

# Set up colour support, if we have it
if [ -t 1 -a -t 2 ]
then
  if tput -V >/dev/null 2>&1
  then
    C_RED=$( tput setaf 1 )
    C_RESET=$( tput sgr0 )
  fi
fi


echo "Clearing Logfile ..."
rm -f "${LOGFILE}"

# Check that the required commands are installed and other conditions are met
# NOte: download tools wget, curl are checked in the download method
echo "Checking ..."
psql --version       >/dev/null 2>>"${LOGFILE}" || exit_error "Postgresql client (psql) is not installed:  Install it now !!!"
#pv --version         >/dev/null 2>>"${LOGFILE}" || exit_error "the pv utility is not installed:  Install it now !!!"
wget --version       >/dev/null 2>>"${LOGFILE}" || exit_error "the wget utility is not installed:  Install it now !!!"

# These tests fail on the MAC.  there is no --version option to the commands
#mktemp --version     >/dev/null 2>&1 || exit_error "mktemp is not installed:  Install it now !!!"
#stat --version        >/dev/null 2>&1 || exit_error "stat is not installed:  Install it now !!!"
#uname --version       >/dev/null 2>&1 || exit_error "uname is not installed:  Install it now !!!"

#export PG_AUTH_DATA="${PGHOST}:${PGPORT}:${PGDATABASE}:${PGUSER}:${PGPASSWORD}"
#PGPASSFILE=${PGPASSFILE:-"${HOME}/.pgpass"}
## Check that we have a .PGPASSFILE in our home directories and that it has the correct permissions
#if [ -e "${PGPASSFILE}" ]
#then
#    [ -O "${PGPASSFILE}" ] && [ -G "${PGPASSFILE}" ] || exit_error "You are not the owner and group of your ${PGPASSFILE} file"
#    # "stat" works very differently on the MAC and Linux, so we will try
#    case "$( uname )" in
#    "Linux")
#            [ "$( stat --format=%a "${PGPASSFILE}" )" = "600" ]  || exit_error "${PGPASSFILE} must have permissions = 600"
#            ;;
#    "Darwin")
#            [ "$( stat -f %p "${PGPASSFILE}" )" = "100600" ]  || exit_error "${PGPASSFILE} must have permissions = 600"
#            ;;
#    *)
#            exit_error "Unidentified operation system: $( uname )"
#    esac
#
#    grep --silent "^${PG_AUTH_DATA}$" "${PGPASSFILE}" || echo "${PG_AUTH_DATA}" >> "${PGPASSFILE}"
#else
#    # Create the correct password file
#    echo "${PG_AUTH_DATA}" > "${PGPASSFILE}"
#    chmod 600 "${PGPASSFILE}"
#fi


if [ -z "${FLAG_DISABLE_DOWN_LOAD_OR_SELECT}" ]
then
    # Check if the dumpfile is current and present
    # As it is named with today's date, it must be current
    if  [ -s "${DUMP_DIR}/${DUMP_FILE}" ]
    then
        LAST_DUMP_FILE="${DUMP_FILE}"
        echo "Today's dump file ${LAST_DUMP_FILE} is already present in ${DUMP_DIR}"
    else
        # see if there is an existing dump file and see if the user want to use it
        LAST_DUMP_FILE="$( ls -1 "${DUMP_DIR}/${PGDATABASE}"*".dump"  2>> "${LOGFILE}" | tail -n 1 )"

        if [ -n "${LAST_DUMP_FILE}" ]
        then
            LAST_DUMP_FILE="$( basename "${LAST_DUMP_FILE}")"  # remove directory
            echo "Your latest dumpfile is ${LAST_DUMP_FILE}:"
        else
            echo "There is no dumpfile present"
        fi
    fi

    if [ -n "${LAST_DUMP_FILE}" ]
    then
        while true
        do
            read -r -p "Do you want to use this file or download a new dump (Use/Download)? " yn
            case ${yn} in

                [Uu]* )
                    # overwrite today's dumpfile with the name of the last dump file
                    DUMP_FILE="${LAST_DUMP_FILE}"
                    break ;;          # Use the existing dump file

                [Dd]* )
                    select_download
                    break ;;

                * ) echo "Please answer (U)se or (D)ownoad";;
            esac
        done
    else
        select_download
    fi
fi

# If we get here then the download was successful, or we are using the last dumpfile

# Remove old dumpfiles leaving only the 2 latest
echo "clearing old dumpfiles - leaving the last 2"
ls -1r "${DUMP_DIR}/${PGDATABASE}"*".dump" | tail -n +3 | xargs ${XARGS_OPTIONS} rm --  # get a list of files excluding the first 2 (ie: starting on line 3)


# If the database exists, check if it is in use and if not drop it
if check_db_exists
then
  echo "checking if database: ${PGDATABASE} is in use"
  # here pgsql is not displaying the headers, and we are selecting only one value.  STDOUT will get the count only, but it will contain extra white space
  ROW_COUNT=$( psql --tuples-only --no-align --host localhost --username "${PGUSER}" --dbname "postgres" --command "SELECT count(*) FROM PGstat_activity WHERE datname='${PGDATABASE}' AND trim(application_name) != '' AND pid != PGbackend_pid();" )
  # remove white space
  ROW_COUNT="$(echo -e "${ROW_COUNT}" | tr -d '[:space]')"

  if [ "${ROW_COUNT}" != "0" ]
  then
      echo
      echo "The following applications are using the database:"
      # This query will detect and show which apps are using a postgres database, and their ids (excluding the current query connection)
      psql --host localhost --username "${PGUSER}" --dbname "postgres" --command "SELECT application_name, pid FROM PGstat_activity WHERE datname='${PGDATABASE}' AND trim(application_name) != '' AND pid != PGbackend_pid();"
      echo
      exit_error "Database in use: The above process must be terminated to import the database"
  fi
fi

# drop the database
echo "Removing the existing database: ${PGDATABASE} (if it exists)"
dropdb --if-exists "${PGDATABASE}" >> "${LOGFILE}" 2>&1 || exit_error "dropdb ${PGDATABASE} failed: Perhaps DB is in use, usually pgadmin3 or rails s/c"

# Attempt to drop the role if it exists exist
#echo "Removing the user role: ${PGUSER} (if it exists)"
#dropuser --if-exists "${PGUSER}" >> "${LOGFILE}" 2>&1  || exit_error "dropuser ${PGUSER} failed"

# Create the user.  Cant use the createuser utility because of password entry
# Using PSQL instead.
echo "Attempting to add user role: ${PGUSER}"
if psql --command "CREATE ROLE ${PGUSER} WITH SUPERUSER LOGIN PASSWORD '${PGPASSWORD}'" >> /dev/null 2>&1
then
  echo "Added user role: ${PGUSER}"
else
  echo "User role: ${PGUSER} already exists"
fi
#psql --command "CREATE ROLE ${PGUSER} WITH SUPERUSER LOGIN PASSWORD '${PGPASSWORD}'" >> /dev/null 2>&1  || exit_error "Unable to create user role: ${PGUSER}. Check logfile ${LOGFILE}"

# create the database
echo "Creating the database: ${PGDATABASE}"
createdb --owner="${PGUSER}" --encoding=utf8 "${PGDATABASE}" >> "${LOGFILE}" 2>&1 || exit_error "createdb failed"

echo "Adding heroku schema 'heroku_ext' ..."
psql "${PGDATABASE}" -c "CREATE SCHEMA IF NOT EXISTS heroku_ext" 2>&1 || exit_error "create schema 'heroku_ext' failed"

echo "Adding heroku schema '_heroku' ..."
psql "${PGDATABASE}" -c "CREATE SCHEMA IF NOT EXISTS _heroku" 2>&1 || exit_error "create schema '_heroku' failed"

echo "Setting postgress search_path to public ..."
psql "${PGDATABASE}" -c "ALTER database "${PGDATABASE}" SET search_path TO public,heroku_ext,_heroku" 2>&1 || exit_error "setting search_path failed"

echo "Restoring dump file to local DB  database=${PGDATABASE} user=${PGUSER} ..."
cat "${DUMP_DIR}/${DUMP_FILE}" | pg_restore --clean --if-exists --no-owner --no-privileges --role="${PGUSER}" --dbname="${PGDATABASE}" >> "${LOGFILE}" 2>&1 || exit_error "pg_restore failed"

if [ -z "${FLAG_DISABLE_POST_PROCESSING}" ]
then
    echo "installing gems"
    bundle install >> "${LOGFILE}" 2>&1 || exit_error "error running 'bundle install'"

    echo "installing yarn components"
    yarn install --check-files >> "${LOGFILE}" 2>&1 || exit_error "error running 'yarn install'"

    # try to migrate if necessary
    echo "running migrations"
    bundle exec rails db:migrate >> "${LOGFILE}" 2>&1 || exit_error "error running 'rails db:migrate'"
fi

#echo "adding universal admin user - login: user@domain.com, passwd: password"
#psql --host localhost "--username=${PGUSER}" -d "${PGDATABASE}" -c "insert into admin_users (name, email, encrypted_password, created_at, updated_at) values('Universal User', 'user@domain.com', '\$2a\$10\$UpJLb0NdYi1IZWOtN0g5hejCq3Z6h8Cjo7LpZdPEQzWxTwc78GLgG', now(), now())" >> "${LOGFILE}" 2>&1 || exit_error "psql failed inserting universal admin user"

echo "finished!"
