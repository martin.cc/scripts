#!/bin/bash

# Download the latest dump file from heroku and store it in the dumps directory

# this is run from cron, and there should never be two process running



# VARIABLES

C_RED=
C_RESET=
TODAY=`date '+%Y-%m-%d'`

PGSERVER='localhost'
PGPORT='5432'
PGUSER='middle8'
PGDATABASE='m8-skills-development'

LOGFILE="/tmp/$(basename $0).log"
HEROKU_APP="m8-skills"
APP_DIR="./m8skills"  # either a FQ path, or relitive the to $HOME directory
DUMP_DIR="${HOME}/pg_dump_files"
DUMP_HISTORY_DAYS=2

# set up the environment, this holds custom changes adn can over write the current variables defined earlier
[ -f ".cron_profile" ]      && source ./.cron_profile       # cron profile last so that it takes precedence

# determine if we should be silent
# if stdout or stderror are not connected to a terminal - use silent
SILENT='true'
if [ -t 1 -a -t 2 ]
then
    unset SILENT
fi

if [ "${SILENT}" ]
then
    CURL_OPTIONS='--silent'
    WGET_OPTIONS="--quiet  --append-output ${LOGFILE}"
    # Set up colour support, if we have it
    if tput -V >/dev/null 2>&1
    then
        C_RED=$(tput setaf 1)
        C_RESET=$(tput sgr0)
    fi
else
    CURL_OPTIONS='--progress'
    WGET_OPTIONS='--progress=bar:noscroll'
fi


# clear out the logfile
[ "${SILENT}" ] || echo 'Clearing Logfile ...'
rm -f ${LOGFILE}

# Clean up
#clean_up()
#{
#    [ "${SILENT}" ] || echo
#    [ "${SILENT}" ] || echo 'cleanup: removing dump file'
#    [ -n "${DUMP_DIR}/${DUMP_FILE}.dump" ] && rm -f ${DUMP_DIR}/${DUMP_FILE}.dump
#}


# Display error and exit
# $1: String to display
exit_error()
{
    # If there is an error we do not want to silence it
    echo >&2
    echo "${C_RED}Error:${C_RESET} $1" >&2
    echo "Error: $1" >> ${LOGFILE}
    echo "Check ${LOGFILE} for more information"  >&2
    echo >&2
    #clean_up
    exit 1
}

# $1 = status
set_status() {

    echo "$1" > ${DUMP_DIR}/${PGDATABASE}_${TODAY}.status
}

download_dump()
{
    set_status 'download_dump'

    [ "${SILENT}" ] || echo "Clearing old dump files"
    #clearing old partials
    if [ ! -s "${DUMP_DIR}/${PGDATABASE}_raw_${TODAY}.partial" ]
    then
        rm -f "${DUMP_DIR}/${PGDATABASE}_raw_"*.partial

    fi

    # Check we are logged in to heroku
    HEROKU_USER=`heroku auth:whoami` || exit_error 'You are not logged in to Heroku: run:  \"heroku login" first'
    # Check we have access to app specified in ${HEROKU_APP}
    (heroku apps | grep ${HEROKU_APP} >>/dev/null 2>&1) || exit_error "Your id: ${HEROKU_USER} does not have access to the '${HEROKU_APP}' app"

    [ "${SILENT}" ] || echo "Locating Dump File ..."
    DUMP_SOURCE=`heroku pg:backups public-url -a ${HEROKU_APP}`
    [ "${SILENT}" ] || echo "Dumpfile: ${DUMP_DIR}/${PGDATABASE}_raw_${TODAY}.partial"
    echo "Dumpfile: ${DUMP_DIR}/${PGDATABASE}_raw_${TODAY}.partial" >> ${LOGFILE}

    # curl --fail --continue-at - -o ${DUMP_DIR}/${PGDATABASE}_raw_${TODAY}.partial --retry 12  ${CURL_OPTIONS}  ${DUMP_SOURCE}  || exit_error "Failed to download Heroku dump file"
    wget --output-document "${DUMP_DIR}/${PGDATABASE}_raw_${TODAY}.partial" --tries=12  ${WGET_OPTIONS} --continue ${DUMP_SOURCE} || exit_error 'Failed to complete download'

    import_dump
}

import_dump() {
    set_status 'import_dump'
    ### Start of Import Dump
    [ "${SILENT}" ] || echo "Restoring dump file to local DB  database=${PGDATABASE} user=${PGUSER} ..."
    dropdb --if-exists --no-password --host ${PGSERVER} --username=${PGUSER} ${PGDATABASE} >> ${LOGFILE} 2>&1  || exit_error 'dropdb failed:  Check if DB is in use, perhaps pgadmin3 or rails s/c'
    createdb --no-password --host ${PGSERVER} --username=${PGUSER} --owner=${PGUSER} ${PGDATABASE} >> ${LOGFILE} 2>&1 || exit_error 'createdb failed'
    pg_restore --no-password --username=${PGUSER}  --no-owner --host=${PGSERVER} --dbname=${PGDATABASE} < "${DUMP_DIR}/${PGDATABASE}_raw_${TODAY}.partial" >> ${LOGFILE} 2>&1 || exit_error 'pg_restore failed'
    clean_db
}

clean_db() {
    set_status "clean_db"
    [ "${SILENT}" ] || echo 'Removing Device Tokens, Delayed Jobs & Eway Tokens ...'
    psql --no-password --host ${PGSERVER} --username=${PGUSER} --dbname ${PGDATABASE} -c 'delete from device_tokens' >> ${LOGFILE} 2>&1 || exit_error 'psql failed removing tokens'
    psql --no-password --host ${PGSERVER} --username=${PGUSER} --dbname ${PGDATABASE} -c 'delete from delayed_jobs'  >> ${LOGFILE} 2>&1 || exit_error 'psql failed removing delayed_jobs'
    psql --no-password --host ${PGSERVER} --username=${PGUSER} --dbname ${PGDATABASE} -c 'update accounts set eway_cc_token=null' >> ${LOGFILE} 2>&1 || exit_error 'psql failed removing credit card tokens'

    # success - move the partial file to the correct name
    mv ${DUMP_DIR}/${PGDATABASE}_raw_${TODAY}.partial ${DUMP_DIR}/${PGDATABASE}_raw_${TODAY}.dump
    rm -f ${DUMP_DIR}/${PGDATABASE}_raw_*.partial   # remove any other partials - dont prompt if there are none to remove

    # try to migrate if necessary
    [ "${SILENT}" ] || echo "running migrations"
    rake db:migrate >> ${LOGFILE} 2>&1 || exit_error "error running 'rake db:migrate'"

    # reset the repository to remove any changes/updates to structure.sql caused by running the migration
    git reset --hard || exit_error "Opps  Can't reset the repository to remove changes introduced by running rake db:migrate"

    [ "${SILENT}" ] || echo "clearing old dump files"
    # Remove dump files that are more than DUMP_HISTORY_DAYS days old
    ls -1r "${DUMP_DIR}/${PGDATABASE}_raw_"*.dump | sed -e "1,${DUMP_HISTORY_DAYS}d" | xargs  rm -f --
    reduce
}

reduce(){
    set_status "reduce"
    #reduce the database
    [ "${SILENT}" ] || echo "Reducing the database"
    rake m8:reduce >> ${LOGFILE} 2>&1 || exit_error "reducing failed"
    anonymise
}

anonymise(){
    set_status "anonymise"
    #Anonymise the database
    [ "${SILENT}" ] || echo "Anonymising the database"
    rake m8:anonymise >> ${LOGFILE} 2>&1 || exit_error "Anonymising failed"
    dump
}

dump(){
    set_status "dump"
    #take a new dump
    [ "${SILENT}" ] || echo "dumping the databse"
    cd ${DUMP_DIR}
    pg_dump -Fc --no-acl --no-owner -h localhost -U "${PGUSER}" "${PGDATABASE}" > "${DUMP_DIR}/${PGDATABASE}_reduced_${TODAY}.dump"

    #removing old dump files
    ls -1r "${DUMP_DIR}/${PGDATABASE}_reduced_"*.dump | sed -e "1,${DUMP_HISTORY_DAYS}d" | xargs  rm -f --
    [ "${SILENT}" ] || echo "database dumped"
    finalise
}

finalise(){
    set_status "finalise"
    [ "${SILENT}" ] || echo "finished!"
}


################################################################
#  Main
################################################################

# Check we have a $HOME, and create the dump directory if it doesn't exist
[ -z "${HOME}" ] && exit_error "You don\'t have a \$HOME variable set"
DUMP_DIR="${HOME}/pg_dump_files"
mkdir -p ${DUMP_DIR}

# This must be run from the home directory.  It is ment to be run frm cron, and this is where is starts
cd ${HOME}


# Check that the required commands are installed and other conditions are met
[ "${SILENT}" ] || echo "Checking ..."
heroku --version     >/dev/null 2>&1 || exit_error "heroku-toolbelt is not installed:  Install it now !!!"
wget --version       >/dev/null 2>&1 || exit_error "wget is not installed:  Install it now !!!"
#curl --version       >/dev/null 2>&1 || exit_error "curl is not installed:  Install it now !!!"
psql --version       >/dev/null 2>&1 || exit_error "psql is not installed:  Install it now !!!"
# These tests fails on the MAC.  there is no --version option to the commands
#mktemp --version     >/dev/null 2>&1 || exit_error "mktemp is not installed:  Install it now !!!"
#id --version         >/dev/null 2>&1 || exit_error "id is not installed:  Install it now !!!"
#stat --version        >/dev/null 2>&1 || exit_error "stat is not installed:  Install it now !!!"
#uname --version       >/dev/null 2>&1 || exit_error "uname is not installed:  Install it now !!!"


# Check that we are not root
[ `id -u` = "0" ] && exit_error "Don't run this command as root"

# make sure rvm is updated, before proceeding
if [[ -s "${HOME}/.rvm/scripts/rvm" ]] ; then
 # First try to load from a user install
 source "${HOME}/.rvm/scripts/rvm"
elif [[ -s "/usr/local/rvm/scripts/rvm" ]] ; then
 # Then try to load from a root install
 source "/usr/local/rvm/scripts/rvm"
else
 exit_error "An RVM installation was not found"
fi

cd "${APP_DIR}"
rvm use

[ "${SILENT}" ] || echo "Updating gems"
bundle >> ${LOGFILE} 2>&1 || exit_error "error running 'bundle install'"

if [ ! -s "${DUMP_DIR}/${PGDATABASE}_${TODAY}.status" ]
then
    rm -f "${DUMP_DIR}/${PGDATABASE}_"*.status
    set_status "download_dump"
fi

STATUS=$(cat "${DUMP_DIR}/${PGDATABASE}_${TODAY}.status")
case ${STATUS} in
download_dump)
    download_dump
    ;;
import_dump)
    import_dump
    ;;
clean_db)
    clean_db
    ;;
reduce)
    reduce
    ;;
anonymise)
    anonymise
    ;;
dump)
    dump
    ;;
finalise)
    finalise
    ;;
esac

