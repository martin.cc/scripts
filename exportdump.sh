#!/bin/bash
#
# This script will create a Postgres database dump
#

# Set up colour support, if we have it
C_RED=
C_RESET=
if tput -V >/dev/null 2>&1
then
    C_RED=$( tput setaf 1 )
    C_RESET=$( tput sgr0 )
fi


# Check that we are not root
[ "$( id -u )" = "0" ] && exit_error "Please don't run this command as root!"

# Check we have a $HOME
[ -z "${HOME}" ] && exit_error "You don't have a HOME environment variable set"


# load the per application parameters
[ -f "./.project_environment.sh" ] || exit_error "Either, you are not running in the application root, or there is no .project_environment file"
source "./.project_environment.sh"

# load the local application parameters if there are any
[ -f "${LOCAL_PROFILE}" ] && source "${LOCAL_PROFILE}"

usage()
{
    echo
    echo "usage: $(basename "$0") <options> <dump file>"
    echo 'options:'
    echo 'If the dumpfile is not specified, the select or download dialog is used'
    echo
}

# Display error and exit
# $1: String to display
exit_error()
{
    echo
    echo "${C_RED}Error:${C_RESET} $1"
    echo "Error: $1" >> "${LOGFILE}"
    echo "Check '${LOGFILE}' for more information"
    echo
    exit 1  #  This will call cleanup
}


echo "LOGFILE=${LOGFILE}"

echo "Clearing Logfile ..."
rm -f "${LOGFILE}"

# create a backup filename
echo "Exporting database: ${PGDATABASE} to ${DUMP_DIR}/${DUMP_FILE}"
pg_dump "${DATABASE}" --clean --no-owner --if-exists --format=custom> "${DUMP_DIR}/${DUMP_FILE}" || exit_error "pg_dump failure."


