#!/bin/bash
# MUST NOT use /bin/sh  - rvm doesn't like the borne shell

# Set up variables
SOURCE_LOCATION="${HOME}/m8_skills"
SOURCE_BRANCH='master'
LOGFILE="/tmp/update_yard.log"

# default cron environment is:
#  HOME = users hoem dir
#  PATH = /bin:/user/bin
#  SHELL = /bin/sh
#  LOGNAME - user login name

# set up the environment, this holds custom changes adn can over write the current variables defined earlier
[ -f ".cron_profile" ] && source ./.cron_profile

# Set up colour support, if we have it
# Are stdout and  stderr both connected to a terminal
# If not then don't set colours
if [ -t 1 -a -t 2 ]
then
    if tput -V >/dev/null 2>&1
    then
        C_RED=`tput setaf 1`
        C_RESET=`tput sgr0`
    fi
fi


# Display error and exit
# $1: String to display
exit_error()
{
    echo >&2
    echo "${C_RED}Error:${C_RESET} $1" >&2
    echo "Error: $1" >> ${LOGFILE}
    echo "Check ${LOGFILE} for more information"  >&2
    echo >&2
    exit 1
}

# This must be run from the home directory
[ "$(pwd)" != "${HOME}" ] && exit_error "This command must be run from the users HOME directory"

# clear the logfile
> ${LOGFILE}

# Enter the project directory
cd ${SOURCE_LOCATION}

# Get rid of any changes that may have been made
git reset --hard HEAD >> ${LOGFILE} 2>&1 || exit_error "unable to run git reset --hard HEAD"

# Make sure we are on master
git checkout ${SOURCE_BRANCH} >> ${LOGFILE} 2>&1 || exit_error "Error: Problem switching git to develop branch"

# Get the latest of develop
git pull >> ${LOGFILE} 2>&1 || exit_error "Error: Problem puling latest develop branch"

# Make sure we have all the required Gems
bundle install >> ${LOGFILE} 2>&1 || exit_error "Error running bundle install"

# get the database upto date
rake db:migrate >> ${LOGFILE} 2>&1 || exit_error "Error running db:migrate"

# Update the docs
yardoc >> ${LOGFILE} 2>&1 || exit_error "Error running yardoc"

# clean up
rm ${LOGFILE}