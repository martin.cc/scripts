#!/usr/bin/env bash

usage() {
  echo "$(basename $0) requires an argument,  postcode, or city, min 3 chars"
  exit -1
}

SILENT='true'
if [ -t 1 -a -t 2 ]
then
    unset SILENT
fi

if [ "${SILENT}" ]
then
    CURL_OPTIONS='--silent'
    WGET_OPTIONS="--quiet  --append-output ${LOGFILE}"
    # Set up colour support, if we have it
    if tput -V >/dev/null 2>&1
    then
        C_RED=$(tput setaf 1)
        C_RESET=$(tput sgr0)
    fi
else
    CURL_OPTIONS='--progress'
    WGET_OPTIONS='--progress=bar:noscroll'
fi

exit_error()
{
    # If there is an error we do not want to silence it
    echo >&2
    echo "${C_RED}Error:${C_RESET} $1" >&2
    echo "Error: $1" >> "${LOGFILE}"
    echo "Check ${LOGFILE} for more information"  >&2
    echo >&2
    #clean_up
    exit 1
}
[ $# -ne 1 ] && usage

wget --version       > /dev/null 2>&1 || exit_error "wget is not installed:  Install it now !!!"
xmllint --version    > /dev/null 2>&1 || exit_error "xmllint is not installed:  Install it now !!! - Its in the 'libxml2-utils' package"

wget -q -O - \
     --header=AUTH-KEY:13d8175d-66d4-4a0f-88f6-f2568163e70f \
     https://auspost.com.au/api/postcode/search.xml?q=$1 | \
     xmllint --format -
