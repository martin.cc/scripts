#!/bin/bash

# This script will dump all the databases with some exemptions
# that end with _production

DUMP_DIR="/pg_dump_files"
LOGFILE="/tmp/$(basename $0).log"   # need to be in a writable directory

echo "LOGFILE: ${LOGFILE}"
echo "Clearing Logfile ..."
rm -f "${LOGFILE}"

# Set up colour support, if we have it
C_RED=
C_RESET=
if tput -V >/dev/null 2>&1
then
    C_RED=$( tput setaf 1 )
    C_RESET=$( tput sgr0 )
fi

# NOTE the MAC doesnt support the --no-run-if-empty flag, but does the action by default, so we only need this for linux
XARGS_OPTIONS=''
if [ "$(uname -s)" = "Linux" ]
then
    XARGS_OPTIONS='--no-run-if-empty'
fi

# Display error and exit
# $1: String to display
exit_error()
{
    echo
    echo "${C_RED}Error:${C_RESET} $1"
    echo "Error: $1" >> "${LOGFILE}"
    echo "Check '${LOGFILE}' for more information"
    echo
    exit 1  #  This will call cleanup
}


# first find the docker container id of the running postgres database
echo "Get postgres container Id"
POSTGRES_CONTAINER_ID=$( (docker container ls  | grep srv-captain--postgresql | awk '{print $1}') 2>>"${LOGFILE}" )
[ -n "${POSTGRES_CONTAINER_ID}" ] || exit_error "Unable to get the postgres container Id"

echo "POSTGRES_CONTAINER_ID: ${POSTGRES_CONTAINER_ID}"

echo "Get list of databases"
DATABASES=$( \
             docker exec "${POSTGRES_CONTAINER_ID}" psql \
               --username=postgres \
               --command='SELECT datname FROM pg_database WHERE datistemplate = false;' \
               --tuples-only \
               2>>"${LOGFILE}" || exit_error "Unable to get list of databases"
          )

# The date string that we will append to each dump name
TODAY=$( date "+%Y-%m-%d" )

# iterate over each database and make a backup
echo "Backing up databases"
for DATABASE in ${DATABASES}
do
  # take a backup
  DUMP_FILE="${DATABASE}_raw_${TODAY}.dump"
  echo "Creating ${DUMP_FILE}"
  docker exec "${POSTGRES_CONTAINER_ID}" pg_dump "${DATABASE}" \
    --username=postgres \
    --clean --no-owner --if-exists --format=custom \
    --file="${DUMP_DIR}/${DUMP_FILE}" \
    2>>${LOGFILE} \
    || exit_error "pg_dump: unable to backup database: ${DATABASE}"

  # check the database we have a get rid of old backups
  echo "clearing old backups for '${DATABASE}' - leaving the last 7"
  # get a list of files excluding the first 7 (ie: starting on line 8) and remove the rest
  find "${DUMP_DIR}" -name "${DATABASE}_raw_*.dump" | sort -r | tail -n +8 | xargs ${XARGS_OPTIONS} rm --  # get a list of files excluding the first 2 (ie: starting on line 3)
done
