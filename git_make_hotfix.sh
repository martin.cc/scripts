#!/usr/bin/env bash

# expect 1 argument
MESSAGE=$1


# This environment var will prevent git for asking for merge messages
export GIT_MERGE_AUTOEDIT=no


# Set up colour support, if we have it
# Are stdout and  stderr both connected to a terminal, If not then don't set colours
if [ -t 1 -a -t 2 ]
then
  if tput -V >/dev/null 2>&1
  then
    C_RED=`tput setaf 1`
    C_GREEN=`tput setaf 2`
    C_BROWN=`tput setaf 3`
    C_BLUE=`tput setaf 4`
    C_RESET=`tput sgr0`
  fi
fi

# get the current directory that this script is in.
# we assume that the other scripts are in the same directory
SYNC_SCRIPT=git_sync.sh
SCRIPT_DIR=`dirname $0`

exit_error ()
{
  # dont display if string is zero length
  [  -z "$1" ] || echo "${C_RED}Error: ${C_BROWN} $1 ${C_RESET}"
  exit 1
}


# check arguments
[ ! -z "${MESSAGE}" ] || exit_error "You must pass a message when making a hotfix"

# make sure we are in a git tree
[ "`git rev-parse --is-inside-work-tree`" = "true" ] || exit_error "NOT a git repository"
echo "Repository check OK"

CURRENT_BRANCH=`git branch | awk '/^\*/{print $2}'`

# check that we are on develop or master
#[ "${CURRENT_BRANCH}" = "master" -o "${CURRENT_BRANCH}" = "develop" ] || exit_error "You MUST be on either the master or development branch"
#echo "Branch check OK"

# check that the current branch is NOT clean (we need changes to make a hotfix)
[  ! -z "`git status --porcelain`" ] || exit_error "Working copy is clean - no hotfix"
echo "Working copy has hotfix ready OK"

########################
# Before doing anything, add the files and then run the commit hook
# Then stash any changes - working directory will be clean after this
########################
git add .
./hooks/pre-commit || exit_error "remove crap from the commit - then run again"
echo "Changes clean"
git stash save "git_make_hotfix: ${MESSAGE}"
echo "Changes stashed OK"

# Git Sync
${SCRIPT_DIR}/${SYNC_SCRIPT}

######################
# GET VERSION NUMBER
######################
# Get the last tag version - only consider tags that are in the correct format
VERSION=`git tag  | grep '^v[0-9]\{1,2\}\.[0-9]\{2\}\.[0-9]\{2,3\}$' | sort | tail -1`
# Verify its format #
# [[ "${VERSION}"  =~ ^v[0-9]{1,2}.[0-9]{2}.[0-9]{2,3}$ ]] || exit_error "Bad version format, version=${VERSION}"
# extract the version components
IFS=. read MAJOR_VERSION MINOR_VERSION PATCH_VERSION <<< "${VERSION}"
# inc the patch version.  note: force base 10 as leading 0 makes bash think this is octal
PATCH_VERSION=$((10#${PATCH_VERSION} + 1))
# add a leading 0's if we need it
[ "${#PATCH_VERSION}" -eq "1" ] && PATCH_VERSION="00${PATCH_VERSION}"
[ "${#PATCH_VERSION}" -eq "2" ] && PATCH_VERSION="0${PATCH_VERSION}"
# verify that it is 3 digits
[[ "$PATCH_VERSION"  =~ ^[0-9]{3}$ ]] || exit_error "New patch version (${PATCH_VERSION}) overflow - will have more that 3 digits"
# assemble the new version
NEW_VERSION="${MAJOR_VERSION}.${MINOR_VERSION}.${PATCH_VERSION}"

######################
# update develop
######################
git checkout develop || exit_error "Failed to checkout develop branch"
echo "Switched to develop"
git merge origin/develop || exit_error "Failed to merge develop from origin"
echo "Develop branch upto date"

######################
# update master
######################
git checkout master || exit_error "Failed to checkout master branch"
echo "Switched to master"
git merge  origin/master || exit_error "Failed to merge master from origin"
echo "Master branch upto date"


# start the hotfix
git flow hotfix start "${NEW_VERSION}" || exit_error "Failed to start hotfix ${NEW_VERSION}"
HOTFIX_BRANCH="hotfix/${NEW_VERSION}"
echo "Branch ${HOTFIX_BRANCH} started"

# commit the stashed changes
git stash pop || exit_error "Failed to pop the stash into the hotfix"
git commit -a -m"${MESSAGE}" || exit_error "Failed to commit the stash to ${HOTFIX_BRANCH}"
echo "Changes committed to ${HOTFIX_BRANCH}"

# close the hotfix
git flow hotfix finish "${NEW_VERSION}" -m"${MESSAGE}" || exit_error "Failed to close ${HOTFIX_BRANCH}"
echo "Branch ${HOTFIX_BRANCH} closed"
# closing the hotfix leaves you on the development branch
# return to the master ???? or should we stay on development
git checkout master || exit_error "Failed to return to master branch"

# now push the changes
${SCRIPT_DIR}/${SYNC_SCRIPT}

