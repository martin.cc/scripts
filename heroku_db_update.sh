#!/bin/sh
#
# This script will update the DB on heroku app, but taking a copy from the production DB
#

# VARIABLES
C_RED=
C_RESET=

HEROKU_UPDATE_TARGET=$1
HEROKU_PRODUCTION="m8-skills"

# Set up colour support, if we have it
if tput -V >/dev/null 2>&1
then
    C_RED=`tput setaf 1`
    C_RESET=`tput sgr0`
fi


# Display error and exit
# $1: String to display
exit_error()
{
    echo
    echo "${C_RED}Error:${C_RESET} $1"
    echo
    exit 1  #  This will call cleanup
}

#Check usage
[ -n "${HEROKU_UPDATE_TARGET}" ] || exit_error "Usage:  heroku_db_update <heroku application name>"

#Check that we are not updating production
[ "${HEROKU_UPDATE_TARGET}" != "${HEROKU_PRODUCTION}" ] || exit_error "Trying to update production - don't do that"


# Check that the required commands are installed and other conditions are met
echo "Checking ..."
heroku --version     >/dev/null 2>&1 || exit_error "heroku-toolbelt is not installed:  Install it now !!!"
# heroku plugins is not displaying all the installed plugins - so I am disabling thsi check for now
#heroku plugins | grep heroku-releases-retry || exit_error "run 'heroku plugins:install heroku-releases-retry' to install a required heroku plugin"


heroku maintenance:on  -a ${HEROKU_UPDATE_TARGET} || exit_error "Unable to set Maintenance Mode for ${HEROKU_UPDATE_TARGET}"
heroku pg:backups restore `heroku pg:backups public-url -a ${HEROKU_PRODUCTION}` DATABASE -a ${HEROKU_UPDATE_TARGET} --confirm ${HEROKU_UPDATE_TARGET} || exit_error "Could not copy Production DB to ${HEROKU_UPDATE_TARGET}"

echo "clearing Device Tokens and Delayed Jobs"
heroku pg:psql -c "delete from device_tokens" -a ${HEROKU_UPDATE_TARGET} || exit_error "Could not delete Device Tokens from ${HEROKU_UPDATE_TARGET}"
heroku pg:psql -c "delete from delayed_jobs"  -a ${HEROKU_UPDATE_TARGET} || exit_error "Could not delete Delayed Jobs from ${HEROKU_UPDATE_TARGET}"

echo "clearing CC Cards"
heroku pg:psql -c "update accounts set eway_cc_token=null" -a ${HEROKU_UPDATE_TARGET} || exit_error "psql failed removing credit card tokens from ${HEROKU_UPDATE_TARGET}"

echo "retry release"  # This will re-run any migrations and build a new slug
heroku releases:retry -a ${HEROKU_UPDATE_TARGET} || exit_error "error running 'heroku release:retry' - try installing it with 'heroku plugins:install heroku-releases-retry'"

echo "restarting dynos"
heroku restart -a ${HEROKU_UPDATE_TARGET} || exit_error "Unable to restart Dynos on ${HEROKU_UPDATE_TARGET}"

echo "removing maintenance mode"
heroku maintenance:off -a ${HEROKU_UPDATE_TARGET} || exit_error "Unable to remove Maintenance Mode for ${HEROKU_UPDATE_TARGET}"

echo "finished!"
