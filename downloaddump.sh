#!/bin/bash

# Download the latest dump file from heroku and store it in the dumps directory

# this is run from cron, and there should never be two process running



# VARIABLES

C_RED=
C_RESET=
TODAY=`date '+%Y-%m-%d'`

PGHOST='localhost'
PGPORT='5432'
PGUSER='middle8'
PGDATABASE='m8-skills-development'

LOGFILE="/tmp/$(basename $0).log"
HEROKU_APP="hmu-staffroom"
APP_DIR="m8_skills"  # either a FQ path, or relative the to $HOME directory
DUMP_DIR="${HOME}/pg_dump_files"
DUMP_HISTORY_DAYS=2


# set up the environment, this holds custom changes adn can over write the current variables defined earlier
[ -f ".cron_profile" ]      && source ./.cron_profile       # cron profile last so that it takes precedence

# if we already have the correct file - bail - we are done for today
[ -f ${DUMP_DIR}/${PGDATABASE}_raw_${TODAY}.dump ] && exit

# determine if we should be silent
# if stdout or stderror are not connected to a terminal - use silent
SILENT='true'
if [ -t 1 -a -t 2 ]
then
    unset SILENT
fi

if [ "${SILENT}" ]
then
    CURL_OPTIONS='--silent'
    WGET_OPTIONS="--quiet  --append-output ${LOGFILE}"
    # Set up colour support, if we have it
    if tput -V >/dev/null 2>&1
    then
        C_RED=$(tput setaf 1)
        C_RESET=$(tput sgr0)
    fi
else
    CURL_OPTIONS='--progress'
    WGET_OPTIONS='--progress=bar:noscroll'
fi


# clear out the logfile
[ "${SILENT}" ] || echo 'Clearing Logfile ...'
rm -f ${LOGFILE}


# Display error and exit
# $1: String to display
exit_error()
{
    # If there is an error we do not want to silence it
    echo >&2
    echo "${C_RED}Error:${C_RESET} $1" >&2
    echo "Error: $1" >> ${LOGFILE}
    echo "Check ${LOGFILE} for more information"  >&2
    echo >&2
    #clean_up
    exit 1
}


download_dump()
{

    [ "${SILENT}" ] || echo "Clearing old dump files"
    #clearing old partials
    if [ ! -s "${DUMP_DIR}/${PGDATABASE}_raw_${TODAY}.partial" ]
    then
        rm -f "${DUMP_DIR}/${PGDATABASE}_raw_"*.partial

    fi

    # Check we are logged in to heroku
    HEROKU_USER=`heroku auth:whoami` || exit_error 'You are not logged in to Heroku: run:  \"heroku login" first'
    # Check we have access to app specified in ${HEROKU_APP}
    (heroku apps | grep ${HEROKU_APP} >>/dev/null 2>&1) || exit_error "Your id: ${HEROKU_USER} does not have access to the '${HEROKU_APP}' app"

    [ "${SILENT}" ] || echo "Locating Dump File ..."
    DUMP_SOURCE=`heroku pg:backups public-url -a ${HEROKU_APP}`
    [ "${SILENT}" ] || echo "Dumpfile: ${DUMP_DIR}/${PGDATABASE}_raw_${TODAY}.partial"
    echo "Dumpfile: ${DUMP_DIR}/${PGDATABASE}_raw_${TODAY}.partial" >> ${LOGFILE}

    # curl --fail --continue-at - -o ${DUMP_DIR}/${PGDATABASE}_raw_${TODAY}.partial --retry 12  ${CURL_OPTIONS}  ${DUMP_SOURCE}  || exit_error "Failed to download Heroku dump file"
    wget --output-document "${DUMP_DIR}/${PGDATABASE}_raw_${TODAY}.partial" --tries=12  ${WGET_OPTIONS} --continue ${DUMP_SOURCE} || exit_error 'Failed to complete download'

    # sucess - rename the file, and remove any old partials
    mv ${DUMP_DIR}/${PGDATABASE}_raw_${TODAY}.partial ${DUMP_DIR}/${PGDATABASE}_raw_${TODAY}.dump
    rm -f ${DUMP_DIR}/${PGDATABASE}_raw_*.partial   # remove any other partials - dont prompt if there are none to remove

    [ "${SILENT}" ] || echo "clearing old dump files"
    # Remove dump files that are more than DUMP_HISTORY_DAYS days old
    ls -1r "${DUMP_DIR}/${PGDATABASE}_raw_"*.dump | sed -e "1,${DUMP_HISTORY_DAYS}d" | xargs  rm -f --

    # finished
    [ "${SILENT}" ] || echo "finished!"

}



################################################################
#  Main
################################################################

# Check we have a $HOME, and create the dump directory if it doesn't exist
[ -z "${HOME}" ] && exit_error "You don\'t have a \$HOME variable set"
DUMP_DIR="${HOME}/pg_dump_files"
mkdir -p ${DUMP_DIR}

# This must be run from the home directory.  It is ment to be run frm cron, and this is where is starts
cd ${HOME}


# Check that the required commands are installed and other conditions are met
[ "${SILENT}" ] || echo "Checking ..."
heroku --version     >/dev/null 2>&1 || exit_error "heroku-toolbelt is not installed:  Install it now !!!"
wget --version       >/dev/null 2>&1 || exit_error "wget is not installed:  Install it now !!!"
#curl --version       >/dev/null 2>&1 || exit_error "curl is not installed:  Install it now !!!"

# These tests fails on the MAC.  there is no --version option to the commands
#mktemp --version     >/dev/null 2>&1 || exit_error "mktemp is not installed:  Install it now !!!"
#id --version         >/dev/null 2>&1 || exit_error "id is not installed:  Install it now !!!"
#stat --version        >/dev/null 2>&1 || exit_error "stat is not installed:  Install it now !!!"
#uname --version       >/dev/null 2>&1 || exit_error "uname is not installed:  Install it now !!!"


# Check that we are not root
[ `id -u` = "0" ] && exit_error "Don't run this command as root"

# if the file with the correct name is present, then we have a already done the job

download_dump

